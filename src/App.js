import React from "react";
import AppWithRouterAccess from "./AppWithRouterAccess";

function App() {
  return <AppWithRouterAccess />;
}
export default App;
