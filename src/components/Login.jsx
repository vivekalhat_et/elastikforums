import React from "react";
import OktaSignInWidget from "../OktaSignInWidget";
import { Redirect } from "react-router-dom";
import { useOktaAuth } from "@okta/okta-react";
import { Center, useToast } from "@chakra-ui/react";

const Login = ({ config }) => {
  const { oktaAuth, authState } = useOktaAuth();
  const toast = useToast();

  const onSuccess = (tokens) => {
    oktaAuth.handleLoginRedirect(tokens);
    toast({
      title: "Success",
      description: "You are successfully logged in",
      status: "success",
      duration: 5000,
      isClosable: true,
    });
  };

  const onError = (err) => {
    toast({
      title: "Error",
      description: err.message,
      status: "error",
      duration: 3000,
      isClosable: true,
    });
  };

  if (!authState) return null;

  return authState.isAuthenticated ? (
    <Redirect to={{ pathname: "/" }} />
  ) : (
    <Center p="5">
      <OktaSignInWidget
        config={config}
        onSuccess={onSuccess}
        onError={onError}
      />
    </Center>
  );
};
export default Login;
