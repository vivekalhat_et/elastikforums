import { Box, Divider, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Box pos="fixed" bottom="10">
      <Divider />
      <Text mt="5">© 2021 Elastik Forum. All Rights Reserved</Text>
    </Box>
  );
};

export default Footer;
