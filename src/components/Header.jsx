import {
  Flex,
  HStack,
  VStack,
  Spacer,
  Text,
  IconButton,
  Button,
  useToast,
  useColorMode,
  useMediaQuery,
} from "@chakra-ui/react";
import { useOktaAuth } from "@okta/okta-react";
import { Link, useHistory } from "react-router-dom";
import { FaSun, FaMoon } from "react-icons/fa";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import React, { useState } from "react";
import { motion } from "framer-motion";
import Footer from "./Footer";

const Header = () => {
  const MotionFlex = motion(Flex);
  const toast = useToast();
  const history = useHistory();
  const [displayMenu, setDisplayMenu] = useState("none");
  const [notSmallerScreen] = useMediaQuery("(min-width:600px)");
  const { oktaAuth, authState } = useOktaAuth();
  const { colorMode, toggleColorMode } = useColorMode();
  const isDark = colorMode === "dark";

  if (!authState || !oktaAuth) return null;

  const login = async () => {
    history.push("/login");
    setDisplayMenu("none");
  };

  const logout = async () => {
    oktaAuth.signOut();
    toast({
      title: "Success",
      description: "You are successfully logged out",
      status: "warning",
      duration: 5000,
      isClosable: true,
    });
    setDisplayMenu("none");
  };

  const handleCreatePost = () => {
    history.push("/compose");
    setDisplayMenu("none");
  };

  const AuthButton = authState.isAuthenticated ? (
    <Button
      colorScheme="red"
      size="md"
      w={displayMenu === "flex" && "full"}
      onClick={logout}
    >
      Sign Out
    </Button>
  ) : (
    <Button
      colorScheme="green"
      size="md"
      w={displayMenu === "flex" && "full"}
      onClick={login}
    >
      Sign In
    </Button>
  );

  return (
    <Flex
      w="full"
      py="3"
      px={notSmallerScreen ? "7" : "5"}
      borderBottom="1px solid"
      borderColor={isDark ? "whiteAlpha.100" : "lightgray"}
      align="center"
    >
      <Text fontSize={notSmallerScreen ? "2xl" : "xl"} fontWeight="bold">
        <Link to="/">Elastik Forum</Link>
      </Text>
      <Spacer />
      <HStack spacing="5">
        <Flex display={["none", "none", "flex", "flex"]}>
          {authState?.isAuthenticated && (
            <Button
              colorScheme="facebook"
              size="md"
              mr={notSmallerScreen && "3"}
              onClick={handleCreatePost}
            >
              Create Post
            </Button>
          )}
          {AuthButton}
        </Flex>
        <IconButton
          aria-label="open-menu"
          size="sm"
          mr="3"
          icon={<HamburgerIcon />}
          display={["flex", "flex", "none", "none"]}
          onClick={() => setDisplayMenu("flex")}
        />
        <IconButton
          size="sm"
          icon={isDark ? <FaSun /> : <FaMoon />}
          isRound="true"
          onClick={toggleColorMode}
        ></IconButton>
      </HStack>

      <MotionFlex
        display={displayMenu}
        w="100vw"
        bg={!isDark ? "gray.50" : "gray.800"}
        zIndex="20"
        h="50vh"
        pos="fixed"
        top="0"
        left="0"
        initial={{ y: "-200vw" }}
        exit={{ y: "-200vw" }}
        animate={{ y: 0 }}
        transition={{ type: "spring", duration: 1, bounce: "0" }}
      >
        <VStack overflow="auto" align="center" w="full">
          <IconButton
            alignSelf="flex-end"
            mt="2"
            mr="2"
            aria-label="close-menu"
            size="md"
            icon={<CloseIcon />}
            onClick={() => setDisplayMenu("none")}
          />
          <VStack w="full" spacing="5" p="5">
            {authState?.isAuthenticated && (
              <Button
                colorScheme="facebook"
                w="full"
                mr={notSmallerScreen && "3"}
                onClick={handleCreatePost}
              >
                Create Post
              </Button>
            )}
            {AuthButton}
          </VStack>
          <Footer />
        </VStack>
      </MotionFlex>
    </Flex>
  );
};

export default Header;
