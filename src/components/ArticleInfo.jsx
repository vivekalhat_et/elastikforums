import {
  VStack,
  Heading,
  Flex,
  Link,
  useMediaQuery,
  useColorMode,
} from "@chakra-ui/react";
import { Link as RouteLink } from "react-router-dom";
import renderHTML from "react-render-html";

const ArticleInfo = ({ article }) => {
  const [notSmallerScreen] = useMediaQuery("(min-width:600px)");
  const { colorMode } = useColorMode();
  const isDark = colorMode === "dark";

  return (
    <VStack
      spacing="3"
      w="full"
      px="5"
      py={notSmallerScreen ? "7" : "5"}
      align="flex-start"
      borderRadius="md"
      border="1px solid"
      borderColor={isDark ? "whiteAlpha.100" : "lightgray"}
      lineHeight={notSmallerScreen ? "10" : "7"}
    >
      <Heading fontSize="2xl">
        <Link
          as={RouteLink}
          to={`/article/${article._id}`}
          style={{ textDecoration: "none" }}
        >
          {article.BlogTitle}
        </Link>
      </Heading>
      <Flex direction={notSmallerScreen ? "row" : "column"} align="flex-start">
        {renderHTML(article.BlogContent.substring(0, 100))}
        <Link
          as={RouteLink}
          to={`/article/${article._id}`}
          color="blue.500"
          ml={notSmallerScreen && "2"}
        >
          read more
        </Link>
      </Flex>
    </VStack>
  );
};

export default ArticleInfo;
