import React, { useState, useEffect } from "react";
import {
  VStack,
  Center,
  Spinner,
  Link,
  Text,
  useMediaQuery,
} from "@chakra-ui/react";
import { Link as RouteLink } from "react-router-dom";
import ArticleInfo from "../components/ArticleInfo";
import { orderBy } from "lodash";

const Dashboard = () => {
  const [articles, setArticles] = useState(null);
  const [loading, setLoading] = useState(true);
  const [notSmallerScreen] = useMediaQuery("(min-width:600px)");

  useEffect(() => {
    fetch("https://elastik-forums-api.herokuapp.com/blogs_ns")
      .then((res) => res.json())
      .then((data) => {
        const blogs = orderBy(data.blogs, ["createdAt"], ["desc"]);
        setArticles(blogs);
        setLoading(false);
      })
      .catch((err) => console(err));

    // return () => setArticles(null);
  }, []);

  return loading ? (
    <Center h="90vh">
      <Spinner />
    </Center>
  ) : articles === null || articles?.length <= 0 ? (
    <Center h="90vh" p={!notSmallerScreen && "5"}>
      <Text fontSize="lg">
        Blog is empty. Click&nbsp;
        <Link
          as={RouteLink}
          to="/compose"
          size="md"
          fontWeight="semibold"
          color="blue"
          textDecor="underline"
        >
          here
        </Link>
        &nbsp;to add a new post.
      </Text>
    </Center>
  ) : (
    <VStack align="flex-start" p={notSmallerScreen ? "7" : "3"} spacing="8">
      {articles &&
        articles.map((article) => (
          <ArticleInfo article={article} key={article._id} />
        ))}
    </VStack>
  );
};

export default Dashboard;
