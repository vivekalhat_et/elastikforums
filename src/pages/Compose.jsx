import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  VStack,
  HStack,
  Button,
  Spacer,
  Input,
  useToast,
  useMediaQuery,
  useColorMode,
} from "@chakra-ui/react";
import { useOktaAuth } from "@okta/okta-react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const Compose = () => {
  const [notSmallerScreen] = useMediaQuery("(min-width:600px)");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(null);

  const { oktaAuth, authState } = useOktaAuth();

  const history = useHistory();
  const toast = useToast();
  const { colorMode } = useColorMode();
  const isDark = colorMode === "dark";

  useEffect(() => {
    !authState?.isAuthenticated
      ? setUser(null)
      : oktaAuth.getUser().then((info) => setUser(info));

    return () => setUser(null);
  }, [authState?.isAuthenticated, oktaAuth]);

  const handlePostSubmit = () => {
    try {
      setLoading(true);
      if (authState && authState.isAuthenticated) {
        const accessToken = oktaAuth.getAccessToken();
        title.length === 0 || content.length === 0
          ? toast({
              title: "Error",
              description: "Title or content cannot be empty.",
              status: "error",
              duration: 3000,
              isClosable: true,
              variant: "left-accent",
            })
          : fetch("https://elastik-forums-api.herokuapp.com/blogs", {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${accessToken}`,
              },
              body: JSON.stringify({
                BlogTitle: title,
                BlogContent: content,
                BlogCreatedBy: user?.name,
              }),
            })
              .then((res) => {
                setStatus(res.status);
                return res.json();
              })
              .then((data) => {
                status !== 200
                  ? toast({
                      title: "Error",
                      description: data.message,
                      status: "error",
                      duration: 5000,
                      isClosable: true,
                      variant: "left-accent",
                    })
                  : toast({
                      title: "Published Successfully",
                      description: "We've successfully published your post.",
                      status: "success",
                      duration: 5000,
                      isClosable: true,
                      variant: "left-accent",
                    });

                status === 200 && history.push("/");
              })
              .catch((err) =>
                toast({
                  title: "Error",
                  description: err.message,
                  status: "error",
                  duration: 5000,
                  isClosable: true,
                  variant: "left-accent",
                })
              );
      }
    } catch (err) {
      toast({
        title: "Error",
        description: err.message,
        status: "error",
        duration: 5000,
        isClosable: true,
        variant: "left-accent",
      });
    } finally {
      setLoading(false);
    }
  };

  const handlePostCancel = () => {
    history.push("/");
  };

  return (
    <VStack
      align="flex-start"
      p={notSmallerScreen ? "3" : "2"}
      m="2"
      minHeight="container.sm"
    >
      <Input
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="What's on your mind?"
        variant="outline"
      />
      <ReactQuill
        style={{
          color: isDark ? "white" : "black",
          marginTop: "0.8rem",
          height: "28rem",
        }}
        value={content}
        onChange={(e) => setContent(e)}
        theme="snow"
        modules={{
          toolbar: [
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            ["bold", "italic", "underline", "strike", "blockquote"],
            [
              { list: "ordered" },
              { list: "bullet" },
              { indent: "-1" },
              { indent: "+1" },
            ],
            ["link"],
          ],
        }}
      />
      <Spacer />
      <HStack w="full" justify="flex-end">
        <Button colorScheme="red" onClick={handlePostCancel} w="xs">
          Cancel
        </Button>
        <Button
          colorScheme="green"
          onClick={handlePostSubmit}
          w="xs"
          isLoading={loading ? true : false}
          loadingText={loading && "Submitting"}
        >
          Publish
        </Button>
      </HStack>
    </VStack>
  );
};

export default Compose;
