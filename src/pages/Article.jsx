import { useState, useEffect } from "react";
import {
  VStack,
  HStack,
  Flex,
  Divider,
  Text,
  Icon,
  Heading,
  Avatar,
  Spinner,
  Center,
  useMediaQuery,
} from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { BsDot } from "react-icons/bs";
import { useOktaAuth } from "@okta/okta-react";
import moment from "moment";
import renderHTML from "react-render-html";
import readingTime from "reading-time";

const Article = () => {
  const { id } = useParams();
  const { authState, oktaAuth } = useOktaAuth();
  const [notSmallerScreen] = useMediaQuery("(min-width:600px)");
  const [article, setArticle] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (authState && authState.isAuthenticated) {
      const accessToken = oktaAuth.getAccessToken();
      fetch(`https://elastik-forums-api.herokuapp.com/blogs/${id}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setArticle(data.blog);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        })
        .catch((err) => console.log(err));
    }
  }, [authState, id, oktaAuth]);

  return loading ? (
    <Center h="90vh">
      <Spinner />
    </Center>
  ) : (
    <VStack
      align="flex-start"
      px={notSmallerScreen ? "7" : "5"}
      py={notSmallerScreen ? "10" : "5"}
      h="fit-content"
    >
      <VStack align="flex-start" spacing="5" w="full">
        <Heading>{article?.BlogTitle}</Heading>
        <Flex
          align={notSmallerScreen ? "center" : "flex-start"}
          direction={notSmallerScreen ? "row" : "column"}
          lineHeight="10"
        >
          <HStack spacing={notSmallerScreen ? "3" : "1"} align="center">
            <Avatar
              name={article?.BlogCreatedBy}
              size="sm"
              mr={!notSmallerScreen && "1"}
            />
            <Text fontWeight="semibold">{article?.BlogCreatedBy}</Text>
          </HStack>
          {notSmallerScreen && <Icon as={BsDot} mx="3" />}
          <Text>
            Published on&nbsp;
            {moment(article?.createdAt).format("MMM Do YY")}
          </Text>
          {notSmallerScreen && <Icon as={BsDot} mx="3" />}
          {article && readingTime(article.BlogContent).text}
        </Flex>
        <Divider />
        {article && renderHTML(article.BlogContent)}
      </VStack>
    </VStack>
  );
};

export default Article;
