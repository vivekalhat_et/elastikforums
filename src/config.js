const oktaAuthConfig = {
  // Note: If your app is configured to use the Implicit flow
  // instead of the Authorization Code with Proof of Code Key Exchange (PKCE)
  // you will need to add `pkce: false`
  issuer: "https://dev-86810625.okta.com/oauth2/default",
  clientId: "0oa1jvzbmhxo5T6Rj5d7",
  redirectUri: window.location.origin + "/callback",
};

const oktaSignInConfig = {
  baseUrl: "https://dev-86810625.okta.com",
  clientId: "0oa1jvzbmhxo5T6Rj5d7",
  redirectUri: window.location.origin + "/callback",
  idps: [
    { type: "GOOGLE", id: "0oa1ki0wv3xRy5EiF5d7" },
    { type: "FACEBOOK", id: "0oa1kgdpld7YCe9Ze5d7" },
  ],
  idpDisplay: "SECONDARY",
  oAuthTimeout: 300000, // 5 minutes
  authParams: {
    // If your app is configured to use the Implicit flow
    // instead of the Authorization Code with Proof of Code Key Exchange (PKCE)
    // you will need to uncomment the below line
    clientId: "0oa1jvzbmhxo5T6Rj5d7",
    redirectUri: window.location.origin + "/callback",
    display: "page",
    responseMode: "fragment",
    responseType: ["token", "id_token"],
    scopes: ["openid", "email", "profile", "address", "phone"],
    state: "WM6D",
    nonce: "YsG76jo",
    issuer: "https://dev-86810625.okta.com/oauth2/default",
    authorizeUrl: "https://dev-86810625.okta.com/oauth2/default/v1/authorize",
    pkce: true,
  },

  // Additional documentation on config options can be found at https://github.com/okta/okta-signin-widget#basic-config-options
};

export { oktaAuthConfig, oktaSignInConfig };
